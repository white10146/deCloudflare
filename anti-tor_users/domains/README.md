# Kontraŭ-Tor uzantoj FQDN-listo


[//]: # (do not edit me; start)

## *18,251* FQDN

[//]: # (do not edit me; end)


- Cloudflare-domajnoj estas ekskluditaj de ĉi tiu esplorado ĉar tiuj estas sufiĉe malamikaj.
- Ne ĉiuj uzantoj de Tor estas malbonaj. Ne punu ĉiujn.
  - Kiel vi sentas, se iu blokas vin sen kialo?
  - Uzi Tor ne estas krimo.
  - [Fakuloj diras, ke gruppuno estas senutila, kontraŭproduktiva, mallaborema kaj neetika](https://web.archive.org/web/20201112000414/https://mypointexactly.wordpress.com/2009/07/21/group-punishment-ineffective-unethical/).
- Blokado de Tor ne estas solvo. Estas VPNj, retprogramoj kaj prokuroj.


-----

# Anti-Tor users FQDN list

- Cloudflare domains are excluded from this research because those are hostile enough.
- Not all Tor users are bad. Do not punish everyone.
  - How do you feel if someone block you for no reason?
  - Using Tor is not a crime.
  - Experts say that group punishment is ineffective, counterproductive, lazy and unethical.
- Blocking Tor is not a solution. There are VPNs, network program and proxies.

-----

# Using the list

  - [Karma API](../../subfiles/service/karma_api.md)
  - [API files for IsMM/IsAT Add-ons](../../tool/api_for_ismm_isat/README.md)


-----

![](../../image/anonexist.jpg)


| Date | FQDN |
| --- | --- |
| 2022-10-23 | 18,251 |
| 2022-10-22 | 17,408 |
| 2022-10-21 | 16,685 |
| 2022-10-20 | 15,883 |
| 2022-10-18 | 14,294 |
| 2022-10-17 | 13,459 |
| 2022-10-16 | 12,866 |
| 2022-10-15 | 12,314 |
| 2022-10-14 | 11,571 |
| 2022-10-13 | 10,718 |
| 2022-10-12 | 9,674 |
| 2022-10-11 | 8,710 |
| 2022-10-10 | 7,790 |
| 2022-10-09 | 7,133 |
| 2022-10-08 | 6,422 |
| 2022-10-07 | 5,584 |
| 2022-10-06 | 5,121 |
| 2022-10-05 | 5,072 |
| 2022-10-04 | 4,958 |
| 2022-10-03 | 4,672 |
| 2022-10-02 | 4,629 |
| 2022-10-01 | 4,575 |
| 2022-09-30 | 4,523 |
| 2022-09-29 | 4,419 |
| 2022-09-28 | 4,370 |
| 2022-09-27 | 4,312 |
| 2022-09-26 | 4,267 |
| 2022-09-25 | 4,216 |
| 2022-09-24 | 4,154 |
| 2022-09-23 | 4,104 |
| 2022-09-22 | 4,061 |
| 2022-09-21 | 4,012 |
| 2022-09-20 | 3,966 |
| 2022-09-19 | 3,918 |
| 2022-09-18 | 3,148 |
| 2022-09-17 | 2,796 |
| 2022-09-16 | 2,743 |
| 2022-09-15 | 2,682 |
| 2022-09-14 | 2,636 |
| 2022-09-13 | 2,576 |
| 2022-09-12 | 2,514 |
| 2022-09-11 | 2,425 |
| 2022-09-10 | 2,375 |
| 2022-09-09 | 2,311 |
| 2022-09-08 | 2,254 |
| 2022-09-07 | 2,188 |
| 2022-09-06 | 2,115 |
| 2022-09-05 | 2,025 |
| 2022-09-04 | 1,953 |
| 2022-09-03 | 1,905 |
| 2022-09-02 | 1,826 |
| 2022-09-01 | 1,734 |
| 2022-08-31 | 1,632 |
| 2022-08-30 | 1,474 |
| 2022-08-29 | 1,367 |
| 2022-08-28 | 1,303 |
| 2022-08-27 | 1,238 |
| 2022-08-26 | 1,142 |
| 2022-08-25 | 1,029 |
| 2022-08-24 | 907 |
| 2022-08-23 | 771 |
| 2022-08-22 | 653 |
| 2022-08-21 | 558 |
| 2022-08-20 | 477 |
| 2022-08-19 | 172 |