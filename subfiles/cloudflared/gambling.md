## How many % of gambling domains are using Cloudflare?


We downloaded the gambling list from [here](https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/gambling-hosts) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 985 |
| eu | 47 |
| net | 43 |
| ag | 39 |
| uk | 34 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 617 |
| Normal | 791 |


### 43.8% of gambling domains are using Cloudflare.