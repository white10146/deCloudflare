## How many % of pornographic domains are using Cloudflare?


We downloaded the pornhosts list from [here](https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/pornography-hosts) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 9,564 |
| net | 1,265 |
| org | 342 |
| pro | 329 |
| tv | 270 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 6,049 |
| Normal | 8,769 |


### 40.8% of pornographic domains are using Cloudflare.