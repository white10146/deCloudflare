## How many % of public Matrix services are using Cloudflare?


- [Matrix](https://en.wikipedia.org/wiki/Matrix_(protocol))
```
Matrix is an open standard and communication protocol for real-time communication. 
It aims to make real-time communication work seamlessly between different service providers, 
in the way that standard Simple Mail Transfer Protocol email currently does for store-and-forward email service, 
by allowing users with accounts at one communications service provider to communicate with users of a different 
service provider via online chat, voice over IP, and videotelephony.
```


Here's a list of public Matrix services.


[//]: # (start replacement)


- Matrix

| Service | Cloudflared |
| --- | --- |
| 0wnz.at | No |
| agdersam.no | No |
| im.aletheia.moe | Yes |
| riot.allmende.io | No |
| alternanet.fr | Yes |
| anonymousland.org | No |
| matrix.arcticfoxes.net | Yes |
| aria.im | No |
| asra.gr | No |
| matrix.bachgau.social | No |
| matrix.badstuebner.biz | No |
| bancino.net | No |
| breeze.town | No |
| buyvm.chat | No |
| matrix.bytes4u.de | No |
| matrix.byzero.dev | Yes |
| matrix.cat.casa | No |
| matrix.catgirl.cloud | Yes |
| matrix.cezeri.tech | Yes |
| mx.chagai.website | No |
| matrix.chatcloud.net | No |
| matrix.chatwave.org | No |
| chrrreeeeesss.com | No |
| club1.fr | No |
| matrix-client.comm.cx | Yes |
| matrix.community.rs | Yes |
| converser.eu | No |
| matrix.crossbach.de | No |
| matrix.data.haus | No |
| im.deuxfleurs.fr | No |
| matrix.duesen.chat | No |
| matrix.envs.net | No |
| exarius.org | No |
| matrix.fachschaften.org | No |
| feneas.org | No |
| matrix.flieger.chat | No |
| matrix.foss.wtf | No |
| matrix.freiburg.social | No |
| freitrix.de | No |
| matrix.fsfe.org | No |
| fulda.social | No |
| g24.at | No |
| matrix.gemeinsam.jetzt | No |
| ggc-project.de | Yes |
| matrix.glasgow.social | No |
| group.lt | Yes |
| hackerspaces.be | No |
| matrix.hadoly.fr | No |
| chatserver.hashi.sbs | Yes |
| hot-chilli.im | No |
| houtworm.im | No |
| synapse.hyteck.de | No |
| inetd.xyz | No |
| matrix.jonasled.de | Yes |
| junta.pl | No |
| chat.librem.one | No |
| matrix.libreon.fr | No |
| matrix.llamarific.social | No |
| lugnsk.org | No |
| chat.magdeburg.jetzt | No |
| mailstation.de | No |
| mandragot.org | No |
| matrix.im | No |
| matrix.org | Yes |
| matrix.monero.social | No |
| mtrx.nz | No |
| nitro.chat | No |
| nltrix.net | No |
| nope.chat | No |
| matrix.ohai.su | No |
| matrix.ombreport.info | No |
| pcriot.org | Yes |
| perthchat.org | Yes |
| matrix.pikaviestin.fi | No |
| pragma-messenger.ch | No |
| www.privacytools.io | Yes |
| privex.io | Yes |
| matrix.pub.solar | No |
| matrix.qoto.org | No |
| radiowarnerd.org | No |
| riotchat.de | Yes |
| ru-matrix.org | No |
| matrix.sans-nuage.fr | No |
| matrix.sfunk1x.com | No |
| matrix.sibnsk.net | No |
| socialnetwork24.com | No |
| matrix.studichat.de | No |
| synod.im | No |
| matrix.tchncs.de | No |
| matrix.tedomum.net | No |
| the-apothecary.club | No |
| matrix.tomesh.net | No |
| trygve.me | No |
| ungleich.matrix.ungleich.cloud | No |
| matrix.unredacted.org | Yes |
| utwente.io | No |
| mtrx.vern.cc | No |
| matrix.virto.community | Yes |
| webchat.weho.st | No |
| xmr.se | No |
| matrix.yatrix.org | No |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 20 |
| Normal | 81 |


### 19.8% of public Matrix services are using Cloudflare.
