## How many % of WHOIS services are using Cloudflare?


We downloaded the WHOIS Domains from [IANA](https://www.iana.org) and filter out duplicates.
Here's the result.

[//]: # (start replacement)

- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 18 |
| Normal | 476 |


### 3.6% of WHOIS services are using Cloudflare.
