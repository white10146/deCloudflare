# Ombrelo


![](../../image/ss-sercxi.png)


**Ombrelo** (pronounce /omˈbrelo/) is the **world's first anti-cloudflare search engine** website primarily for [Tor](https://torproject.org) [users](http://ombrelo.x66j7jej74efeulffzy3hu3p4dtfruiwb3vv6ec7b5bxlkogxw5vnpid.onion/) and [clearnet](https://en.wikipedia.org/wiki/Clearnet_(networking)) [users](https://ombrelo.eu.org/), operating since the [year 2016](https://addons.thunderbird.net/en-us/firefox/addon/searxes/). The primary focus of the project is to provide people with better search results which do not [waste their time](../../INSTRUCTION.md#website-is-rejecting-tor-visitor).

It is fed by quality sources and rank down [Cloudflare sites](../cloudflare_users/domains) to bottom by default, thus you [avoid the risk](../README.md) and [inconvenience](../PEOPLE.md) of having [MITM traps](../README.md) littered throughout search results.
You can also open a cached version of the page by clicking the icon of the search result.
There are many options to choose from for example disabling Cloudflare ranking, rank down known [Tor-hostile](../anti-tor_users/domains) sites and display additional details.
They are also providing public API service for developers and also Lynx-site for [text-base browser](../readme/en.ethics.md#browser-vendor-discrimination) users.

The current logo resembles an [umbrella](https://en.wiktionary.org/wiki/ombrelo). Developers said that they want to provide an _umbrella_ to protect people safe against heavy _rain_ caused by the _cloud_.


![](../../image/ssprotect.jpg)
