## Block Cloudflare Requests (Palemoon)

`Cancel requests to Cloudflare sites`


This add-on is for [Palemoon browser](https://en.wikipedia.org/wiki/Pale_Moon_(web_browser)). (Firefox [here](../../subfiles/about.bcma.md))  


- This **does NOT** mean we endorse Palemoon. [Learn about wolfbeast](../../cloudflare_users/README.md).

- [Download for Palemoon](https://git.disroot.org/dCF/deCloudflare/raw/branch/master/tool/block_cloudflare_requests_pm/bcfr.pm.xpi)

- [LICENSE](LICENSE)
