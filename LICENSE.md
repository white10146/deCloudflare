# [Crimeflare #deCloudflare](http://crimeflare.eu.org)

```
Crimeflare #deCloudflare http://crimeflare.eu.org
```

## **Beware of modified mirror sites.**
- **Please try to link to above URL instead of git. ([Why](HISTORY.md#other))**


-----


### Permesilo

* `/addons/*` -- [MIT](addons/LICENSE)
* `/pdf/*` -- Nekonata (Vi povas trovi ekzempleron ĉie. Dankon al aŭtoroj.)
* `/tool/block_cloudflare_mitm_fx/*` -- [MIT](tool/block_cloudflare_mitm_fx/LICENSE.md)
* `/tool/block_cloudflare_requests_pm/*` -- [MIT](tool/block_cloudflare_requests_pm/LICENSE)
* Alio -- [CC BY 4.0](https://web.archive.org/web/https://creativecommons.org/licenses/by/4.0/)
 **nur se la permesilo ne estas specifita!**


[Ĉi tiu deponejo](http://crimeflare.eu.org) estis kreita anonime, publike, por uzi la mondon [kontraŭstari](https://dw.expert/2020/06/13/the-dark-side-of-google-interview-with-ex-employee-of-the-company-zach-vorhies/) [Cloudflare](https://www.cloudflare.com/).
  
Kontribuantoj, kiuj anonime [kontribuis](HISTORY.md) (inkluzive en [CryptoParty](https://cryptoparty.at/cryptoparty_wien_53)), venis antaŭen por doni al ĉi tiu projekto sian benon.


-----


### License

* `/addons/*` -- [MIT](addons/LICENSE)
* `/pdf/*` -- Unknown (You can find a copy everywhere. Thanks to authors)
* `/tool/block_cloudflare_mitm_fx/*` -- [MIT](tool/block_cloudflare_mitm_fx/LICENSE.md)
* `/tool/block_cloudflare_requests_pm/*` -- [MIT](tool/block_cloudflare_requests_pm/LICENSE)
* Else -- [CC BY 4.0](https://web.archive.org/web/https://creativecommons.org/licenses/by/4.0/) **only if the license is not specified!**


[This repository](http://crimeflare.eu.org) was created anonymously, in public, for the use of the world to [resist](https://dw.expert/2020/06/13/the-dark-side-of-google-interview-with-ex-employee-of-the-company-zach-vorhies/) [Cloudflare](https://www.cloudflare.com/).
  
Contributors who have anonymously [contributed](HISTORY.md) (including in [CryptoParty](https://cryptoparty.at/cryptoparty_wien_53)) had since come forward to give this project their blessing.

What are you waiting for? Contribute!